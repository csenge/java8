import java.util.*;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {

        Class myCLassInstance = Main.class;

        //JVM - print class loader for class created by me
        System.out.println(myCLassInstance.getClassLoader());

        //JVM - see how this gets translated by JVM - for java 11 up you can replace List with var
        List myList = new ArrayList();
        myList.add(new Test().doStg());

        //create optional value
        Optional<String> myOptional = Optional.of("Csenge");

        //check if optional is present
        System.out.println(myOptional.isPresent());

        //check if optional is present - if yes print something if not print something else. Use isPresent
        if (myOptional.isPresent()) {
            System.out.println("The optional is present with the value:"
                    + myOptional.get());
        } else {
            System.out.println("Optional missing");
        }

        //check if optional is present - if yes print something if not do nothing. Use ifPresent. Takes as argument a consumer functional interface
        myOptional.ifPresent(x ->
                System.out.println("Optional value obtained with consumer " + x));

        //create your consumer - functional interface implemented by anonymous class
        Consumer<String> optionalConsumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println("Optional value obtained " +
                        "with custom consumer " + s);
            }
        };

        //use your own consumer to check presence of value
        myOptional.ifPresent(optionalConsumer);

        Map<String, Integer> productMap = new HashMap();
        productMap.put("Cola", 4);
        productMap.put("Cola", 2);
        productMap.put("Fanta", 3);

        //merge method for adding values in map. arguments: key, value, bifunction that takes the original value and the new one and executes a method using there two parameters
        //Integer::sum --> method reference. sum is a method declared in the Integer class, takes two parameters
        productMap.merge("Cola", 5, Integer::sum);
        productMap.merge("Cappy", 5, Integer::sum);

        //print the elements of the map - usual way
        for (String productKey: productMap.keySet() ) {
            System.out.println(productKey + " : " + productMap.get(productKey));
        }

        //get on an element that does not exist - return default value set by developer
        System.out.println(productMap.getOrDefault("Nimic", -1));

        //stream over map elements
        productMap.keySet().stream().forEach(productKey ->
                System.out.println(productKey + " : " +
                        productMap.get(productKey)));

    }

}
