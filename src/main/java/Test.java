import lombok.*;
//import lombok.extern.slf4j.Slf4j;

//import java.util.logging.Level;

//constructor with no params
@NoArgsConstructor
//constructor with all params
@AllArgsConstructor
//builder pattern
@Builder
//Logging option 2
//@Slf4j
public class Test {

    //Logging option 1
//    Logger logger = Logger.getLogger(Test.class.getName());

    @Getter
    @Setter
    private String testName;

    public String doStg() {
     //   log(Level.INFO, "Testing the logger");
        return "done";
    }
}
